drop table if exists Tour cascade;
drop table if exists City cascade;
drop table if exists Review cascade;
drop table if exists CityToTour cascade;

create table Tour
(
  name varchar(128) primary key
);

create table City
(
  name varchar(128) primary key
);

create table Review
(
  "comment" varchar(128),
  rating integer,
  tour varchar(128) references Tour(name)
);

create table CityToTour
(
  "tourName" varchar(128),
  "cityName" varchar(128),
  constraint "FK_nameCity_toCity" foreign key("cityName") references City(name),
  constraint "FK_nameTour_toTour" foreign key("tourName") references Tour(name)
);

insert into Tour
values  ('В гостях у Хаски'),
    ('Хиты карелии');

insert into City
values  ('Приозерск'),
    ('Сартовала'),
    ('Валаам');

insert into CityToTour
values  ('В гостях у Хаски', 'Приозерск'),
    ('В гостях у Хаски', 'Сартовала'),
    ('Хиты карелии', 'Сартовала'),
    ('Хиты карелии', 'Валаам');

insert into Review
values  ('Отличный тур', 5, 'Хиты карелии'),
    ('Не понравилось', 2, 'Хиты карелии');

    select "tourName", "cityName", comment, rating from Tour
           join CityToTour on Tour.name = CityToTour."tourName"
           join City on City.name = CityToTour."cityName"
           join Review on Tour.name = Review.tour;
